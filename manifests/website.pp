# Manage what's needed to serve and build the Tails website,
# glueing together other tails::website::* resources.
#
# Know caveats: "ensure => absent" cleans up only partially.

class tails::website (
  Enum['present', 'absent'] $ensure  = present,
  Stdlib::Fqdn $public_hostname      = 'tails.boum.org',
  String $user                       = 'tails-website',
  Stdlib::Absolutepath $home_dir     = "/srv/${public_hostname}",
  String $gitolite_pubkey_name       = 'gitolite@puppet-git.lizard',
  Hash $po_slave_languages           = $tails::website::params::production_slave_languages,
  Boolean $is_staging                = false,
) inherits tails::website::params {

  $config_dir    = "${home_dir}/config"
  $git_dir       = "${home_dir}/git"
  $web_dir       = "${home_dir}/html"
  $underlays     = [
    'etcher-binary',
    'mirror-pool',
    'mirror-pool-dispatcher',
    'promotion-material',
    'uui-binary',
  ]
  $git_repos     = ['tails'] + $underlays
  $src_dir       = "${git_dir}/tails/wiki/src"
  $url           = "https://${public_hostname}"
  $cgi_url       = "${url}/ikiwiki.cgi"

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  file { $home_dir:
    ensure => $directory_ensure,
    owner  => root,
    group  => $user,
    mode   => '0751',
  }

  file { $git_dir:
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0750',
  }

  file { $web_dir:
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0755',
  }

  user { $user:
    ensure   => $ensure,
    home     => $home_dir,
    password => '*',
    require  => Group[$user],
  }

  group { $user:
    ensure => $ensure,
  }

  file { "${home_dir}/.ssh":
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }

  sshkeys::set_client_key_pair { 'tails-website@www':
    user    => $user,
    home    => $home_dir,
    require => File["${home_dir}/.ssh"],
  }

  $git_repos.each |String $repo| {
    vcsrepo { "${git_dir}/${repo}":
      ensure     => $ensure,
      provider   => git,
      source     => "git@gitlab-ssh.tails.boum.org:tails/${repo}.git",
      owner      => $user,
      user       => $user,
      group      => $user,
      submodules => false,
      require    => [
        Package['git'],
        File[$git_dir],
        Sshkeys::Set_client_key_pair['tails-website@www'],
      ],
    }
    # Ensure permissions are corrected if someone mistakenly did something
    # as root in this checkout
    file { "${git_dir}/${repo}/.git/index":
      owner   => $user,
      group   => $user,
      require => Vcsrepo["${git_dir}/${repo}"],
    }
  }

  class { '::tails::website::builder':
    with_search => true,
  }

  file { $config_dir:
    ensure => $directory_ensure,
    owner  => root,
    group  => $user,
    mode   => '0750',
  }

  file { "${config_dir}/ikiwiki.setup":
    ensure  => $ensure,
    owner   => root,
    group   => $user,
    mode    => '0640',
    content => template('tails/website/ikiwiki.setup.erb'),
    notify  => Exec['update ikiwiki wrappers'],
  }

  exec { 'update ikiwiki wrappers':
    command     => "ikiwiki --setup '${config_dir}/ikiwiki.setup' --refresh --wrappers",
    user        => $user,
    cwd         => $home_dir,
    environment => [ "HOME=${home_dir}" ],
    refreshonly => true,
    require     => [
      Class['::tails::website::builder'],
      File[$web_dir],
      Vcsrepo["${git_dir}/tails"],
    ],
  }

  tails::website::webserver::instance { $public_hostname:
    ensure  => $ensure,
    web_dir => $web_dir,
  }

  sshkeys::set_authorized_keys { $gitolite_pubkey_name:
    user    => $user,
    home    => $home_dir,
    options => [
      'restrict',
      'pty',
      'command="/usr/local/bin/tails-website-update-underlays"',
    ],
  }

  file { '/usr/local/bin/tails-website-update-underlays':
    ensure  => $ensure,
    content => template('tails/website/update-underlays.erb'),
    owner   => root,
    group   => root,
    mode    => '0755',
  }

  class { '::tails::website::rss2email':
    public_hostname => $public_hostname,
    user            => $user,
    home_dir        => $home_dir,
    email_recipient => 'amnesia-news@boum.org',
  }

}
