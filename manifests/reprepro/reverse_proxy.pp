# Manage a nginx vhost that reverse proxies one of our APT repositories to another web server.
define tails::reprepro::reverse_proxy (
  Stdlib::Fqdn $public_hostname,
  String $onion_hostname            = '',
  Stdlib::Httpurl $upstream_address = 'http://apt.lizard/',
  ) {

  nginx::vhostsd { $public_hostname:
    content => template('tails/reprepro/nginx_reverse_proxy.erb'),
    require => [
        Package[nginx],
        Tails::Letsencrypt::Certonly[$public_hostname],
    ],
  }

  tails::letsencrypt::certonly { $public_hostname: }

}
