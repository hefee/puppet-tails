# Manage resources shared by all Tails website instances
class tails::website::webserver::common (
  Enum['present', 'absent'] $ensure = 'present',
  Hash $po_slave_languages          = $tails::website::params::production_slave_languages,
) inherits tails::website::params {

  assert_private()

  nginx::confd { 'map_lang':
    ensure  => $ensure,
    content => template('tails/website/nginx/map_lang.erb'),
  }

  include tails::website::webserver::rewrite_rules

  $packages = [
    'fcgiwrap',
    'libnginx-mod-http-fancyindex',
  ]

  ensure_packages($packages)

  $service_ensure = $ensure ? {
    absent  => stopped,
    default => running,
  }

  $service_enable = $ensure ? {
    absent  => false,
    default => true,
  }

  service { 'fcgiwrap.socket':
    ensure   => $service_ensure,
    enable   => $service_enable,
    provider => systemd,
    require  => Package['fcgiwrap'],
  }

  # Ensure only the www-data user can use the fcgiwrap socket

  file { '/etc/systemd/system/fcgiwrap.socket.d':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { '/etc/systemd/system/fcgiwrap.socket.d/puppet-tails.conf':
    ensure => $ensure,
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  $fcgiwrap_systemd_socket_settings = {
    'SocketUser'  => 'www-data',
    'SocketGroup' => 'www-data',
    'SocketMode'  => '0600',
  }

  $fcgiwrap_systemd_socket_settings.each |String $setting, String $value| {
    ini_setting { "fcgiwrap ${setting}":
      ensure  => $ensure,
      path    => '/etc/systemd/system/fcgiwrap.socket.d/puppet-tails.conf',
      section => 'Socket',
      setting => $setting,
      value   => $value,
      notify  => Service['fcgiwrap.socket'],
      require => File['/etc/systemd/system/fcgiwrap.socket.d/puppet-tails.conf'],
    }
  }

  file { '/usr/local/bin/tails-website-last-month-stats':
    ensure => $ensure,
    source => 'puppet:///modules/tails/website/tails-website-last-month-stats',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
}
