# Manage backupninja
class tails::backupninja (
  Boolean $reportsuccess = false,
) {

  # Resources

  ensure_packages(['backupninja'])

  file_line { 'backupninja_reportsuccess':
    path    => '/etc/backupninja.conf',
    match   => '#?reportsuccess\s*=.*',
    line    => sprintf(
      'reportsuccess = %s',
      bool2str($reportsuccess, 'yes', 'no')
    ),
    require => Package['backupninja'],
  }

}
