# This class manages a tails mirror.
#
# == Notes ==
#
# When class is declared with "ensure => absent" it
# will not uninstall any packages.
#
# == Parameters ==
#
# - $webserver: nginx or apache2, defaults to nginx
#
# - $package: lets you explicitly define the webserver
#   package name. This can be useful in case default
#   packages to be installed conflict with already
#   installed ones. Defaults to webserver's name.
#
# - $server_name: hostname under which the files are served.
#   Defaults to dl.amnesia.boum.org.
#
# - $access_log: access log file path. Defaults to keep
#   no logs.
#
# - $error_log: error log file path. Default to keep
#   no logs.
#
# == Usage ==
#
# You can either include this class:
#
#   include tails::mirror
#
# or declare it and provide parameters:
#
#   class { 'tails::mirror':
#     ensure    => 'present',
#     webserver => 'apache2',
#   }
#

class tails::mirror (
  Enum['present', 'absent'] $ensure = 'present',
  String $webserver                 = 'nginx',
  String $package                   = $webserver,
  Stdlib::Fqdn $server_name         = 'dl.amnesia.boum.org',
  Stdlib::Absolutepath $mirror_path = '/var/www/tails-mirror/',
  String $mirror_owner              = 'www-data',
  String $mirror_group              = 'www-data',
  Stdlib::Filemode $mirror_mode     = '0755',
  Stdlib::Absolutepath $access_log  = '/dev/null',
  Stdlib::Absolutepath $error_log   = '/dev/null',
  Optional[String] $admin_mail      = undef,
) {

  ensure_packages(['rsync', $webserver])

  $dir_ensure = $ensure ? {
    'present' => 'directory',
    'absent'  => 'absent',
  }

  $dir_force = $ensure ? {
    'present' => false,
    'absent'  => true,
  }

  $link_ensure = $ensure ? {
    'present' => 'link',
    'absent'  => 'absent',
  }

  file { $mirror_path:
    ensure  => $dir_ensure,
    mode    => $mirror_mode,
    owner   => $mirror_owner,
    group   => $mirror_group,
    force   => $dir_force,
    content => template('tails/mirror/mirror_cron.erb'),
  }

  file { '/etc/cron.d/tails':
    ensure  => $ensure,
    mode    => '0644',
    owner   => root,
    group   => root,
    content => template('tails/mirror/mirror_cron.erb'),
    require => File[$mirror_path]
  }

  file { "/etc/${webserver}/sites-available/dl.amnesia.boum.org":
    ensure  => $ensure,
    mode    => '0644',
    owner   => root,
    group   => root,
    content => template("tails/mirror/${webserver}/dl.amnesia.boum.org.erb"),
    require => Package[$webserver],
    notify  => Service[$webserver],
  }

  file { "/etc/${webserver}/sites-enabled/dl.amnesia.boum.org":
    ensure  => $link_ensure,
    target  => "/etc/${webserver}/sites-available/dl.amnesia.boum.org",
    require => [
      File["/etc/${webserver}/sites-available/dl.amnesia.boum.org"],
      Service[$webserver]
    ],
  }

  ensure_resource('service', $webserver, {'ensure' => 'running'})
}
