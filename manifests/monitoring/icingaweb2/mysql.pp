# Setup the MySQL database used by icingaweb2
# Icingaweb2 package doesn't use dbconfig (Debian#831532)
class tails::monitoring::icingaweb2::mysql (
  String $web_db_name,
  String $web_db_user,
  String $web_db_pass,
  String $web_user_pass,
  Enum['present', 'absent'] $ensure = present,
){

  assert_private()

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  mysql::db { $web_db_name:
    ensure   => $ensure,
    # XXX: charset and collate look wrong, but that's what "show
    # variables like '%_database'" tells me we have, so let's not mess
    # it up. We might want to convert this database to the current
    # Debian defaults some day (in Stretch: utf8mb4,
    # utf8mb4_general_ci).
    charset  => 'latin1',
    collate  => 'latin1_swedish_ci',
    user     => $web_db_user,
    password => $web_db_pass,
    host     => 'localhost',
    grant    => ['ALL'],
    tag      => "mysql_${::fqdn}",
  }

  file { '/usr/local/sbin/install_icingaweb2_database':
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/monitoring/icingaweb2/scripts/install_icingaweb2_database',
  }

  file { '/var/lib/icingaweb2':
    ensure => $directory_ensure,
    owner  => 'icingaweb2',
    group  => 'icingaweb2',
    mode   => '0755',
  }

  # XXX: replace this, somehow (#12451)
  exec { 'install_web_database':
    user    => 'icingaweb2',
    group   => 'icingaweb2',
    command => "/usr/local/sbin/install_icingaweb2_database '${web_db_name}' '${web_db_user}' '${web_db_pass}' '${web_user_pass}'",
    creates => '/var/lib/icingaweb2/.db_installed',
    require => [
      Mysql::Db[$web_db_name],
      File['/var/lib/icingaweb2'],
    ],
  }

}
