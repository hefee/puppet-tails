class tails::monitoring::checkcommand::torified_smtp (
  Enum['present', 'absent'] $ensure = 'present',
){

  file { '/etc/icinga2/conf.d/torified_smtp.conf':
    ensure => $ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0600',
    source => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/torified_smtp.conf',
  }

}
