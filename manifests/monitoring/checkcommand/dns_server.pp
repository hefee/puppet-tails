class tails::monitoring::checkcommand::dns_server (
  Enum['present', 'absent'] $ensure = 'present',
){

  file { '/etc/icinga2/conf.d/dns_server.conf':
    ensure => $ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0600',
    source => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_dns_server.conf',
  }

}
