class tails::monitoring::checkcommand::ssh_or_sftp_account (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_ssh_or_sftp_account

  file { '/etc/icinga2/conf.d/check_ssh_or_sftp_account.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_ssh_or_sftp_account.conf',
    require => Class['tails::monitoring::plugin::check_ssh_or_sftp_account'],
  }

}
