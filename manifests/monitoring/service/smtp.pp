# Manages the monitoring of a SMTP server.
#
# === Parameters
#
# [*address*]   - FQDN that shall be monitored, defaults to $name.
# [*real_host*] - name of the host where the service is actually running.
# [*ensure*]    - present/absent, defaults to present.

define tails::monitoring::service::smtp (
  String $real_host,
  Stdlib::Fqdn              $address              = $name,
  Enum['present', 'absent'] $ensure               = present,
  String                    $nodename             = 'ecours.tails.boum.org',
  Boolean                   $enable_notifications = true,
  String                    $check_interval       = '10m',
  String                    $retry_interval       = '2m',
){

  file { "/etc/icinga2/conf.d/smtp_${name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/smtp.erb'),
    notify  => Service['icinga2'],
  }

}
