# Manages the monitoring of a nameserver.
#
# === Parameters
#
# [*real_host*]        - name of the host where the service is actually running.
# [*ns_ip*]            - IP address of the nameserver to query.
# [*query_hostname*]   - hostname to query.
# [*ensure*]           - present/absent, defaults to present.
# [*expect_authority*] - boolean, whether the queried nameserver must be authoritative for the lookup, defaults to false.

define tails::monitoring::service::dns (
  String                            $real_host,
  Stdlib::IP::Address::V4           $ns_ip,
  Stdlib::Fqdn                      $query_hostname,
  Enum['present', 'absent']         $ensure               = present,
  Boolean                           $expect_authority     = false,
  String                            $nodename             = 'ecours.tails.boum.org',
  String                            $check_interval       = '5m',
  String                            $retry_interval       = '100s',
  Boolean                           $enable_notifications = true,
){

  file { "/etc/icinga2/conf.d/dns_${name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/dns.erb'),
    notify  => Service['icinga2'],
  }

}
