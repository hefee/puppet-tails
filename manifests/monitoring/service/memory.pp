# Manages the monitoring of system memory.
# Take care to use a unique $name, e.g by including $::fqdn in it.

define tails::monitoring::service::memory (
  String $zone,
  String $nodename,
  Enum['present', 'absent'] $ensure = present,
  Pattern[/\A\d{1,3}\z/] $mem_warning               = '20',
  Pattern[/\A\d{1,3}\z/] $mem_critical              = '10',
  String $check_interval            = '1m',
  String $retry_interval            = '30s',
  Boolean $enable_notifications     = true,
){

  include ::tails::monitoring::plugin::check_mem

  file {"/etc/icinga2/zones.d/${zone}/memory.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/memory.erb'),
    require => Class['tails::monitoring::plugin::check_mem'],
    notify  => Service['icinga2'],
  }

}
