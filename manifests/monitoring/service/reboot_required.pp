# Manages the monitoring of required reboot,
# using reboot-notifier's flag file.

define tails::monitoring::service::reboot_required (
  String $nodename,
  String $zone,
  Enum['present', 'absent'] $ensure = present,
  Stdlib::Absolutepath $flag_file   = '/run/reboot-required',
  Integer[1] $warning_age           = 1, # can't be 0 due to how the plugin is implemented
  Integer[1] $critical_age          = 172800,
  String $check_interval            = '10m',
  String $retry_interval            = '2m',
  Boolean $enable_notifications     = true,
){

  validate_re($name, '^[\w-]+@[\w.-]+$')

  file { "/etc/icinga2/zones.d/${zone}/reboot_required_${name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/reboot_required.erb'),
    notify  => Service['icinga2'],
  }

}
