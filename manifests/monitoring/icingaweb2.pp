# Manages Tails icingweb2 instance running on the master.
# Requires the nginx puppet module and the puppetlabs/stdlib one.
#
class tails::monitoring::icingaweb2 (
  String $ido_db,
  String $ido_db_name,
  String $ido_db_user,
  String $ido_db_pass,
  String $web_db,
  String $web_db_name,
  String $web_db_user,
  String $web_db_pass,
  String $web_user_pass,
  Stdlib::Fqdn $webserver_hostname,
  String $web_admins                = 'icingaadmin',
  Enum['present', 'absent'] $ensure = present,
){

  $link_ensure = $ensure ? {
    absent  => absent,
    default => link,
  }

  class { '::tails::monitoring::icingaweb2::nginx':
    ensure             => $ensure,
    webserver_hostname => $webserver_hostname,
  }

  class { '::tails::monitoring::icingaweb2::mysql':
    ensure        => $ensure,
    web_db_pass   => $web_db_pass,
    web_db_name   => $web_db_name,
    web_db_user   => $web_db_user,
    web_user_pass => $web_user_pass,
  }

  $packages = [
    'icingaweb2-common',
    'icingaweb2-module-monitoring',
    'php-icinga',
  ]

  package { $packages:
    ensure  => $ensure,
    require => Class['::tails::monitoring::icingaweb2::nginx'],
  }

  file { '/etc/icinga2/features-enabled/command.conf':
    ensure => $link_ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0644',
    target => '/etc/icinga2/features-available/command.conf',
  }

  class { '::icingaweb2':
    initialize     => false,
    install_method => 'package',
    ido_db         => $ido_db,
    ido_db_pass    => $ido_db_pass,
    ido_db_name    => $ido_db_name,
    ido_db_user    => $ido_db_user,
    web_db         => $web_db,
    web_db_pass    => $web_db_pass,
    web_db_name    => $web_db_name,
    web_db_user    => $web_db_user,
    admin_users    => $web_admins,
    require        => Package[$packages],
  }

  class { '::icingaweb2::mod::monitoring': }

}
