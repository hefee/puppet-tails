class tails::monitoring::plugin::check_upgradeable (
  Enum['present', 'absent'] $ensure  = 'present',
){

  ensure_packages(['apt-show-versions'], {'ensure' => $ensure})

  file {'/usr/lib/nagios/plugins/check_upgradeable':
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/monitoring/icinga2/plugins/check_upgradeable',
    notify => Service['icinga2'],
  }

}
