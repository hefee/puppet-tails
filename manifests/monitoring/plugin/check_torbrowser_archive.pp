class tails::monitoring::plugin::check_torbrowser_archive (
  Enum['present', 'absent'] $ensure  = 'present',
){

  package { ['python3-lxml', 'python3-urllib3']:
    ensure => $ensure,
  }

  file {'/usr/lib/nagios/plugins/check_torbrowser_archive':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/tails/monitoring/icinga2/plugins/check_torbrowser_archive',
    require => Package['python3-lxml', 'python3-urllib3'],
    notify  => Service['icinga2'],
  }

}
