class tails::monitoring::plugin::check_ssh_or_sftp_account (
  Enum['present', 'absent'] $ensure  = 'present',
){

  package { 'python3-paramiko':
    ensure => $ensure,
  }

  file {'/usr/lib/nagios/plugins/check_ssh_or_sftp_account':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/tails/monitoring/icinga2/plugins/check_ssh_or_sftp_account',
    require => Package['python3-paramiko'],
  }

}
