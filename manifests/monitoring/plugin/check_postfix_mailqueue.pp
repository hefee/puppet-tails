class tails::monitoring::plugin::check_postfix_mailqueue (
  Enum['present', 'absent'] $ensure  = 'present',
){

  file {'/usr/lib/nagios/plugins/check_postfix_mailqueue':
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/monitoring/icinga2/plugins/check_postfix_mailqueue',
    notify => Service['icinga2'],
  }

}
