# Manage Git hooks specific to tails.git
class tails::gitolite::hooks::tails () inherits tails::website::params {

  $hooks_directory = '/var/lib/gitolite3/repositories/tails.git/hooks'

  file { $hooks_directory:
    ensure => directory,
    mode   => '0755',
    owner  => gitolite3,
    group  => gitolite3,
  }

  # TODO: remove after applied for the first time.
  file { "${hooks_directory}/pre-receive":
    ensure => absent,
  }

  file { "${hooks_directory}/post-receive":
    content => template('tails/gitolite/hooks/tails-post-receive.erb'),
    owner   => root,
    group   => root,
    mode    => '0755',
    require => Package[curl],
  }

  # Gitolite's own update hook will automatically chain to update.secondary,
  # taking care to pass it the same 3 arguments the original update hook
  # received from Git.
  $weblate_update_hook_packages = [
    'python3-git',
  ]
  file { "${hooks_directory}/update.secondary":
    source  => 'puppet:///modules/tails/gitolite/hooks/tails-weblate-update.hook',
    owner   => root,
    group   => root,
    mode    => '0755',
    require => Package[$weblate_update_hook_packages],
  }
  ensure_packages($weblate_update_hook_packages)
  file { "${hooks_directory}/langs.json":
    content => template('tails/weblate/langs.json.erb'),
    mode    => '0640',
    owner   => root,
    group   => gitolite3,
  }

  ensure_packages(['curl'])

}
