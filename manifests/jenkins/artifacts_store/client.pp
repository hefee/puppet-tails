# Manage NFS mountpoint for a Jenkins artifacts store client.
class tails::jenkins::artifacts_store::client (
  Stdlib::Absolutepath $path,
  Stdlib::Absolutepath $remote_path,
  Stdlib::Host $nfs_server,
  Enum['present', 'absent'] $ensure = 'present',
){

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  if $ensure == 'present' {
    include nfs::client
  } elsif $ensure == 'absent' {
    package { [
      nfs-common,
      rpcbind,
      portmap
    ]:
      ensure => purged,
    }
  }

  file { $path:
    ensure => $directory_ensure,
    mode   => '0755',
  }

  nfs::mount { 'artifact_store':
    ensure         => $ensure,
    share          => $remote_path,
    mountpoint     => $path,
    server         => $nfs_server,
    client_options => 'auto,nodev,nosuid,noexec',
    server_options => 'ro,root_squash',
    require        => [
      File[$path],
      Package['nfs-common'],
    ],
  }

}
