# Manage a nginx vhost that reverse proxies to a Jenkins instance.
class tails::jenkins::reverse_proxy (
  Stdlib::Fqdn $public_hostname   = 'jenkins.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname = 'jenkins.lizard',
  Stdlib::Port $upstream_port     = 8080,
  ) {

  ### Sanity checks

  if $::osfamily != 'Debian' {
    fail('The tails::jenkins::reverse_proxy class only supports Debian.')
  }

  ### Resources

  $upstream_address = "${upstream_hostname}:${upstream_port}"

  nginx::vhostsd { $public_hostname:
    content => template('tails/jenkins/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      Nginx::Authd['jenkins'],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included['tails_jenkins_reverse_proxy'],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  nginx::included { 'tails_jenkins_reverse_proxy':
    content => template('tails/jenkins/nginx/reverse_proxy.inc.erb'),
  }

  nginx::authd { 'jenkins':
    source  => "puppet:///modules/site_jenkins/nginx/${::fqdn}/auth.d/htpasswd.jenkins",
  }

  tails::letsencrypt::certonly { $public_hostname: }

}
