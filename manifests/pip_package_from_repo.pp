define tails::pip_package_from_repo (
  String $version,
  String $url,
  String $tag = $version,
  String $repo_type = 'git',
) {
  exec { "pip_install_${name}":
    command => "pip3 install --ignore-installed --no-deps ${repo_type}+${url}@${tag}#egg=${name}-${version}",
    require => Package[$tails::weblate::python_modules::pip_packages],
    unless  => "/usr/bin/pip3 show '${name}' && [ '${version}' = \$(/usr/bin/pip3 show '${name}' | /usr/bin/awk -F': ' '/^Version: / {print \$2}') ]", # lint:ignore:140chars -- command
  }
}
