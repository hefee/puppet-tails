# Manage Weblate dependencies that are in Debian
class tails::weblate::debian_packages () {

  # some packages are commented out because the needed versions for Weblate
  # 2.19.1 are still not available in Debian stable. Fore more details, see:
  # https://redmine.tails.boum.org/code/issues/10038
  $packages = [
    'ccze',
    'ipython3',  # for more convenient debugging
    'mercurial',  # so we can use pip to install from hg repos
    'python3-dateutil',
    'python3-defusedxml',
    'python3-levenshtein',
    'python3-lxml',
    'python3-memcache',
    'python3-mysqldb',
    'python3-pil',
    'python3-psycopg2',
    'python3-pyuca',
    'python3-six',
    'python3-yaml',
    'python3-whoosh',
    'sqlite3',
  ]

  ensure_packages($packages)

}
