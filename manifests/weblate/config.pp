# Put Weblate configuration in place
class tails::weblate::config(
  Stdlib::Absolutepath $mutable_data_dir,
  Stdlib::Absolutepath $apache_data_dir,
  Stdlib::Absolutepath $code_git_checkout,
  String $db_name,
  String $db_user,
  String $db_password,
  String $weblate_secret_key,
  String $redis_password,
) {

  include tails::weblate::webserver

  ensure_packages(['memcached'])

  service { 'memcached':
    ensure  => running,
    require => Package[memcached],
  }

  file { "${mutable_data_dir}/config":
    ensure => directory,
    owner  => root,
    group  => 'weblate_admin',
    mode   => '2775',
  }


  file { "${mutable_data_dir}/config/settings.py":
    ensure  => present,
    content => template('tails/weblate/settings.py.erb'),
    owner   => 'weblate',
    group   => 'weblate_admin',
    mode    => '0640',  # contains passwords
    notify  => Service[apache2],
  }

  file { "${code_git_checkout}/weblate/settings.py":
    ensure  => symlink,
    target  => "${mutable_data_dir}/config/settings.py",
    require => Vcsrepo[$code_git_checkout],
    notify  => Service[apache2],
  }

}
