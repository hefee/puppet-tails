# Manage Weblate dependencies that are in not in Debian
class tails::weblate::python_modules () {

  $pip_packages = [
    'python3-pip',
    'python3-setuptools',
  ]

  ensure_packages($pip_packages)

  # The following deps are in Debian Stretch but with a version that is older
  # than needed by Weblate 3.0. The versions noted as comments are the
  # requirements for the current Weblate version we are running (check
  # manifests/weblate.pp to know which version it is).

  tails::pip_package_from_repo { 'celery':
    version => '4.3.0',  # >= 4.0
    tag     => 'v4.3.0',
    url     => 'https://github.com/celery/celery',
  }

  tails::pip_package_from_repo { 'celery-batches':
    version => '0.2',  # >= 0.2
    tag     => 'v0.2',
    url     => 'https://github.com/percipient/celery-batches',
  }

  tails::pip_package_from_repo { 'django':
    version => '1.11.27',  # >= 1.11
    url     => 'https://github.com/django/django',
  }

  tails::pip_package_from_repo { 'django-appconf':
    version => '1.0.2',  # >= 1.0
    tag     => 'v1.0.2',
    url     => 'https://github.com/django-compressor/django-appconf',
  }

  tails::pip_package_from_repo { 'django-compressor':
    version => '2.2',  # >= 2.1.1
    url     => 'https://github.com/django-compressor/django-compressor',
    require => Exec['pip_install_django-appconf']
  }

  tails::pip_package_from_repo { 'djangorestframework':
    version => '3.8.2',  # >= 3.8
    url     => 'https://github.com/encode/django-rest-framework',
  }

  tails::pip_package_from_repo { 'filelock':
    version => '3.0.4',  # >= 3.0.1
    tag     => 'v3.0.4',
    url     => 'https://github.com/benediktschmitt/py-filelock',
  }

  tails::pip_package_from_repo { 'jellyfish':
    version => '0.7.2',  # >= 0.6.1
    tag     => '0.7.2',
    url     => 'https://github.com/jamesturk/jellyfish',
  }

  tails::pip_package_from_repo { 'translate-toolkit':
    version => '2.3.1',  # >= 2.3.1
    url     => 'https://github.com/translate/translate',
  }

  # The following deps are in Debian Buster, but not in Stretch.

  tails::pip_package_from_repo { 'django-crispy-forms':
    version => '1.7.2',  # >= 1.7.2
    url     => 'https://github.com/django-crispy-forms/django-crispy-forms',
  }

  tails::pip_package_from_repo { 'oauthlib':
    version => '3.1.0',  # >= 3.0.0
    tag     => 'v3.1.0',
    url     => 'https://github.com/oauthlib/oauthlib',
  }

  tails::pip_package_from_repo { 'openpyxl':
    version   => '2.6.0',  # >= 2.5.0
    url       => 'https://bitbucket.org/openpyxl/openpyxl/src/default',
    repo_type => 'hg',
  }

  tails::pip_package_from_repo { 'phply':
    version => '1.2.4',
    url     => 'https://github.com/viraptor/phply',
  }

  tails::pip_package_from_repo { 'redis':
    version => '3.3.11',  # >= 3.3.11 -- from celery's kombu
    url     => 'https://github.com/andymccurdy/redis-py',
  }

  tails::pip_package_from_repo { 'ruamel.yaml':
    version   => '0.16.5',
    url       => 'https://bitbucket.org/ruamel/yaml/src/default',
    repo_type => 'hg',
  }

  tails::pip_package_from_repo { 'siphashc':
    version => '1.0',  # >= 0.8
    tag     => 'v1.0',
    url     => 'https://github.com/WeblateOrg/siphashc',
  }

  tails::pip_package_from_repo { 'social-auth-core':
    version => '3.1.0',  # >= 3.1.0
    url     => 'https://github.com/python-social-auth/social-core',
  }

  tails::pip_package_from_repo { 'social-auth-app-django':
    version => '3.1.0',  # >= 3.1.0
    url     => 'https://github.com/python-social-auth/social-app-django',
  }

  tails::pip_package_from_repo { 'translation-finder':
    version => '1.7',  # >= 1.0
    url     => 'https://github.com/WeblateOrg/translation-finder.git',
  }

  tails::pip_package_from_repo { 'user-agents':
    version => '1.1.0',  # >= 1.1.0
    tag     => 'v1.1.0',
    url     => 'https://github.com/selwin/python-user-agents',
  }

}
