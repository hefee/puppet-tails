# Configure the database for the translation platform
class tails::weblate::database(
  String $db_name,
  String $db_user,
  String $db_password,
) {

  ### PostgreSQL database configuration

  class { 'postgresql::server': }

  postgresql::server::db { $db_name:
    user     => $db_user,
    password => postgresql_password($db_user, $db_password),
  }

  ### PgBouncer -- a PostgreSQL pooling mechanism

  ensure_packages(['pgbouncer'])

  file { '/etc/pgbouncer/pgbouncer.ini':
    ensure  => present,
    owner   => postgres,
    group   => postgres,
    mode    => '640',
    require => Package['pgbouncer'],
    content => template('tails/weblate/pgbouncer.ini.erb'),
  }

  $md5_password_hash = md5("${db_password}${db_user}")

  file { '/etc/pgbouncer/userlist.txt':
    ensure  => present,
    owner   => postgres,
    group   => postgres,
    mode    => '640',
    require => Package['pgbouncer'],
    content => "\"${db_user}\" \"md5${md5_password_hash}\"",
  }

  service { 'pgbouncer':
    ensure  => running,
    subscribe => [
      File['/etc/pgbouncer/pgbouncer.ini'],
      File['/etc/pgbouncer/userlist.txt'],
    ],
  }

  ### Backups

  include tails::backupninja

  file { '/etc/backup.d/10.pgsql':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "hotcopy = no
sqldump = yes
backupdir = /var/backups/postgres
databases = all
compress = yes
format = plain
",
    require => Package['backupninja'],
  }

}
