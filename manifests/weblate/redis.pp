# Manage a Redis server as a message broker for use of Celery in Weblate
class tails::weblate::redis(
  String $password,
) {

  package { 'redis-server':
    ensure => installed,
  }

  # This was created from the default config file distributed with Debian 9,
  # but modified so Redis is configured to:
  #
  #   - bind to 127.0.0.1 on port 6379 (this is default, but worth mentioning).
  #   - write an append only log every second.
  #   - require an authentication password (tails::weblate::redis::password).

  file { '/etc/redis/redis.conf':
    content => template('tails/weblate/redis.conf.erb'),
    owner   => redis,
    group   => redis,
    mode    => '0640',  # contains Redis password
    require => Package['redis-server'],
    notify  => Service['redis-server'],
  }

  service { 'redis-server':
    ensure   => running,
    provider => systemd,
    require  => File['/etc/redis/redis.conf'],
  }

}
