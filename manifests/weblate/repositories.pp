# Clone and checkout the needed Git repositories
class tails::weblate::repositories(
  Stdlib::Absolutepath $mutable_data_dir,
  Stdlib::Absolutepath $code_git_checkout,
  String $code_git_remote,
  String $code_git_revision,
) {

  file { "${mutable_data_dir}/repositories":
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2770',
  }

  # Repository used by weblate

  vcsrepo { $code_git_checkout:
    provider => git,
    source   => $code_git_remote,
    revision => $code_git_revision,
    user     => root,
  }

  vcsrepo { "${mutable_data_dir}/repositories/vcs/tails/index":
    ensure   => present,
    provider => git,
    remote   => 'origin',
    user     => weblate,
    group    => weblate,
    source   => {
      # used by Weblate
      'origin' => 'gitolite@puppet-git.lizard:tails',
      # update_weblate_components.py, that operates on this working copy,
      # needs to keep track of the status of the remote repo independently
      # of Weblate
      'cron'   => 'gitolite@puppet-git.lizard:tails',
    },
  }

  # Repository used by our scripts that update Git outside of Weblate

  vcsrepo { "${mutable_data_dir}/repositories/integration":
    ensure   => present,
    provider => git,
    remote   => 'origin',
    user     => weblate,
    group    => weblate,
    source   => {
      # used to merge/push changes from/to the canonical repo
      'origin'  => 'gitolite@puppet-git.lizard:tails',
      # used to fetch the commits that Weblate created
      'weblate' => "${mutable_data_dir}/repositories/vcs/tails/index/.git",
    },
    require  => Vcsrepo["${mutable_data_dir}/repositories/vcs/tails/index"],
  }

  # Ensure Git configuration on repositories

  $git_checkouts = [
    "${mutable_data_dir}/repositories/vcs/tails/index",
    "${mutable_data_dir}/repositories/integration",
  ]

  $git_checkouts.each |String $git_checkout| {

    $git_config = "${git_checkout}/.git/config"

    file { $git_config:
      owner   => weblate,
      group   => weblate,
      mode    => '0640',
      require => Vcsrepo[$git_checkout],
    }

    create_ini_settings(
      {
        'user' => {
          'name'  => 'Tails translators',
          'email' => 'tails-l10n@boum.org',
        },
      },
      {
        path    => $git_config,
        require => File[$git_config],
      }
    )
  }

}
