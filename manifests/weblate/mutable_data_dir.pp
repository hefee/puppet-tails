# Manage a directory needed by many Weblate resources
class tails::weblate::mutable_data_dir(
  Stdlib::Absolutepath $mutable_data_dir,
) {

  file { $mutable_data_dir:
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2750',
  }

  file { "${mutable_data_dir}/.ssh":
    ensure => directory,
    owner  => weblate,
    group  => weblate,
    mode   => '2770',
  }

}
