# Send reminder email to contributors
class tails::redmine::reminder (
  Hash $users,
  String $api_key,
  String $email_sender,
) {

  $packages = [
    'python3-requests',
    'python3-yaml',
  ]

  $users_file = '/etc/redmine/users.yml'
  $api_key_file = '/etc/redmine/reminder-api-key'
  $email_body_file = '/etc/redmine/reminder-email-body'

  ensure_packages($packages)

  file { '/usr/local/bin/redmine-remind':
    source  => 'puppet:///modules/tails/redmine/reminder/redmine-remind',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package[$packages],
  }

  file { $api_key_file:
    content => $api_key,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
  }

  file { $email_body_file:
    source => 'puppet:///modules/tails/redmine/reminder/email_body',
    owner  => 'root',
    group  => 'root',
    mode   => '0600',
  }

  file { $users_file:
    content => template('tails/redmine/users.yml.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
  }

  cron { 'redmine-remind':
    command  => "/usr/local/bin/redmine-remind --send-email --api-key='@${api_key_file}' --users-file='${users_file}' --email-body-file='${email_body_file}' --email-from='${email_sender}'", # lint:ignore:140chars -- command
    month    => '*/2',
    monthday => 17,
    hour     => 3,
    minute   => 23,
    require  => [
      File[
        $api_key_file,
        $email_body_file,
        $users_file,
        '/usr/local/bin/redmine-remind',
      ],
    ],
  }

}
