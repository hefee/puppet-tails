# Manage nginx sites for Tails' archived Redmine
define tails::redmine::archive_vhost (
  Boolean $public,
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Fqdn $web_hostname           = $name,
  Optional[String] $http_user          = undef,
  Optional[String] $http_password_hash = undef,
  Stdlib::Absolutepath $web_dir        = "/srv/${web_hostname}",
) {

  ### Sanity checks

  if ! $public and $http_password_hash == undef {
    fail("\$http_password_hash must be set for private archives")
  }

  if ! $public and $http_user == undef {
    fail("\$http_user must be set for private archives")
  }

  ### Resources

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }
  file { [$web_dir, "${web_dir}/code"]:
    ensure => $directory_ensure,
    owner  => root,
    group  => 'www-data',
    mode   => '0750',
  }

  file { "${web_dir}/index.html":
    ensure  => $ensure,
    content => template('tails/redmine/archive/index.html.erb'),
    owner   => root,
    group   => 'www-data',
    mode    => '0640',
  }

  $auth_file_ensure = $ensure ? {
    present => $public ? {
      true    => absent,
      default => present,
    },
    default  => absent,
  }
  nginx::authd { $web_hostname:
    ensure  => $auth_file_ensure,
    content => template('tails/redmine/archive/auth.erb'),
  }

  tails::letsencrypt::certonly { $web_hostname: }

  nginx::vhostsd { $web_hostname:
    ensure  => $ensure,
    content => template('tails/redmine/archive/site.erb'),
    require => [
      File[$web_dir],
      Nginx::Authd[$name],
      Tails::Dhparam['/etc/nginx/dhparams.pem'],
    ],
  }

}
