# Manage redirection from https://redmine.tails.boum.org/ to GitLab
class tails::redmine::redirector (
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Fqdn $redmine_hostname    = 'redmine.tails.boum.org',
  String $gitlab_url                = 'https://gitlab.tails.boum.org',
  Boolean $ssl                      = true,
  Stdlib::Absolutepath $web_dir     = "/srv/${redmine_hostname}",
  String $attachments_source        = 'puppet:///modules/tails_private/redmine/redirector/attachments_rewrite_rules.conf',
) {

  nginx::vhostsd { $redmine_hostname:
    ensure  => $ensure,
    content => template('tails/redmine/redirector/site.erb'),
    require => [
      Package[nginx],
      File[$web_dir],
      Tails::Dhparam['/etc/nginx/dhparams.pem'],
      Nginx::Included['tails_redmine_redirector_attachments'],
    ],
  }

  nginx::included { 'tails_redmine_redirector_attachments':
    ensure => $ensure,
    source => $attachments_source,
  }

  if $ssl {
    tails::letsencrypt::certonly { $redmine_hostname: }
  }

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  file { $web_dir:
    ensure => $directory_ensure,
    owner  => root,
    group  => 'www-data',
    mode   => '0750',
  }

}
