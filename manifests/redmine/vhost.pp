# Manage an Apache VirtualHost for Tails' Redmine instance
define tails::redmine::vhost (
  Stdlib::Fqdn $public_hostname          = $name,
  Stdlib::IP::Address::V4 $ip            = $::ipaddress,
  Boolean $ssl                           = true,
  Boolean $monitoring                    = true,
  Boolean $ddosmode                      = false,
  Optional[String] $ddosmode_destination = undef,
) {

  ### Sanity checks

  if $ddosmode and $ddosmode_destination == undef {
    fail('$ddosmode_destination must be defined when $ddosmode is enabled.')
  }

  ### Resources

  file { "/etc/apache2/sites-enabled/${public_hostname}.conf":
    content => template('tails/redmine/apache/vhost.conf.erb'),
    notify  => Service['apache'],
  }

  $monitoring_ensure = $monitoring ? { true => present, default => absent }
  @@::tails::monitoring::service::http { $public_hostname:
    ensure    => $monitoring_ensure,
    ssl       => $ssl,
    ip        => $ip,
    real_host => $::fqdn,
  }

}
