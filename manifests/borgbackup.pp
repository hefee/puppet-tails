# Manage common pieces needed by our backups

class tails::borgbackup (
  String $borgpassphrase,
) {

  ensure_packages(['borgbackup','libguestfs-tools','moreutils'])

  ['runbackuplv.sh', 'runbackupfs.sh'].each |String $script| {
    file { "/usr/local/sbin/${script}":
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      source  => "puppet:///modules/tails/borgbackup/${script}",
      require => File['/etc/tails-borgbackup/secret'],
    }
  }

  # create a configuration directory for our excludes and secret files
  # in a place that avoids possible confusion with borgbackups's
  # own configuration directory in /etc/borgbackup

  file { '/etc/tails-borgbackup':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
  }

  file { '/etc/tails-borgbackup/secret':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => $borgpassphrase,
  }

}
