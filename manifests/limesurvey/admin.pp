# Manage credentials for users who admin LimeSurvey
define tails::limesurvey::admin () {

  user::groups::manage_user { "${name}_in_limesurvey":
    user    => $name,
    group   => 'limesurvey',
    require => Group['limesurvey'],
  }

  user::groups::manage_user { "${name}_in_limesurvey_admin":
    user    => $name,
    group   => 'limesurvey_admin',
    require => Group['limesurvey_admin'],
  }

}
