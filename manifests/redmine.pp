# Manage Tails' Redmine instance
class tails::redmine (
  String $mailhandler_api_key,
  String $redmine_ws_api_key   = 'secret',
  String $gitolite_pubkey_name = 'gitolite@puppet-git.lizard',
  Stdlib::Httpsurl $url        = 'https://redmine.tails.boum.org/code',
  Boolean $ddosmode            = false,
  String $ddosmode_destination = 'sxkh7umwgc2rutlr.onion',
  Hash $virtual_hosts          = {
    'redmine.tails.boum.org' => {
      'ip'         => '127.0.0.1',
      'ssl'        => false,
      'monitoring' => false,
    },
    ## Block Redmine access from the outside
    ## https://salsa.debian.org/tails-team/gitlab-migration/-/issues/43
    # 'sxkh7umwgc2rutlr.onion' => {
    #   'ip'         => '127.0.0.1',
    #   'ssl'        => false,
    #   'monitoring' => false,
    # },
  },
  Hash $apache_settings        = {
    'KeepAlive' => 'Off',
    'Timeout'   => '120',
  },
) {

  include ::apache
  include ::apache::base
  include ::mysql::server

  # the tails_git user owns the tails.git repository that is linked in redmine

  user { 'tails_git':
    ensure => present,
    system => true,
    shell  => '/usr/bin/git-shell',
    home   => '/srv/repositories',
    gid    => 'www-data',
  }

  file { '/srv/repositories':
    ensure => directory,
    owner  => 'tails_git',
    group  => 'www-data',
    mode   => '2750',
  }

  vcsrepo { '/srv/repositories/tails.git':
    ensure   => mirror,
    provider => git,
    source   => 'https://git-tails.immerda.ch/tails',
    user     => 'tails_git',
    group    => 'www-data',
    require  => File['/srv/repositories'],
  }

  file { '/srv/repositories/tails.git/hooks/post-update':
    content => template('tails/redmine/tails-post-update.hook.erb'),
    mode    => '0700',
    owner   => 'tails_git',
    group   => 'www-data',
    require => Vcsrepo['/srv/repositories/tails.git'],
  }

  sshkeys::set_authorized_keys { $gitolite_pubkey_name:
    user => 'tails_git',
    home => '/srv/repositories',
  }

  file { '/var/cache/debconf/redmine.preseed':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0600',
    source => 'puppet:///modules/tails/redmine/redmine.preseed',
  }

  package { 'redmine':
    ensure       => installed,
    responsefile => '/var/cache/debconf/redmine.preseed',
    require      => File['/var/cache/debconf/redmine.preseed'],
  }

  # We need:
  # - passenger for redmine
  # - qos to mitigate overenthusiastic clients
  # - git to link to the tails.git repo

  ensure_packages([
    'git',
  ])

  $virtual_hosts.each |String $public_hostname, Hash $vhost_params| {
    tails::redmine::vhost { $public_hostname:
      ip                   => $vhost_params['ip'],
      ssl                  => $vhost_params['ssl'],
      ddosmode             => $ddosmode,
      ddosmode_destination => $ddosmode_destination,
      monitoring           => $vhost_params['monitoring'],
    }
  }

  file { '/etc/apache2/sites-enabled/server-status.conf':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/tails/redmine/apache/sites/server-status.conf',
    notify => Service['apache'],
  }

  $apache_config_file = '/etc/apache2/apache2.conf'
  $apache_settings.each |String $setting, String $value| {
    file_line { "apache_${setting}":
      path   => $apache_config_file,
      line   => "${setting} ${value}",
      match  => "^${setting}\s+",
      notify => Service['apache'],
    }
  }

  file { '/etc/apache2/mods-available/mpm_worker.conf':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/tails/redmine/apache/mods/mpm_worker.conf',
    notify => Service['apache'],
  }

  file { '/etc/apache2/mods-available/reqtimeout.conf':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/tails/redmine/apache/mods/reqtimeout.conf',
    notify => Service['apache'],
  }

  $apache_modules = [
    'passenger',
    'qos',
    'removeip',
  ]
  $apache_modules.each |String $module| {
    ensure_packages(["libapache2-mod-${module}"])
    exec { "a2enmod ${module}":
      creates => "/etc/apache2/mods-enabled/${module}.load",
      require => Package["libapache2-mod-${module}"],
      notify  => Service['apache'],
    }
  }

  file { '/etc/apache2/mods-available/qos.conf':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0644',
    source  => 'puppet:///modules/tails/redmine/apache/mods/qos.conf',
    require => Package['libapache2-mod-qos'],
  }

  augeas {
    'logrotate_redmine':
      context => '/files/etc/logrotate.d/redmine/rule',
      changes => [
        'set file /var/log/redmine/*/*.log',
        'set rotate 7',
        'set schedule daily',
        'set compress compress',
        'set missingok missingok',
        'set ifempty notifempty',
        'set copytruncate copytruncate',
      ],
      require => Package['redmine'],
  }

  tails::dpkg_statoverride { '/usr/share/redmine/log':
    user  => 'root',
    group => 'www-data',
    mode  => '775',
  }

  file { '/etc/logrotate.d/redmine_git_hosting':
    source => 'puppet:///modules/tails/redmine/logrotate.d/redmine_git_hosting',
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  vcsrepo { '/usr/share/redmine/plugins/redmine_mentions':
    provider => git,
    source   => 'https://github.com/planio-gmbh/redmine_mentions.git',
    revision => '1a09938e58ce97482b14ff1dfaed9fe9392cb57e',
    user     => 'root',
    require  => Package['redmine'],
    notify   => Service['apache'],
  }

  file { '/usr/share/redmine/public/themes/Modula Mojito':
    ensure  => directory,
    recurse => true,
    source  => 'puppet:///modules/tails/redmine/themes/Modula_Mojito';
  }

  # Reverting www-data back to Debian defaults, this used to be different
  user { 'www-data':
    ensure => present,
    home   => '/var/www',
    shell  => '/usr/sbin/nologin',
    system => true,
    uid    => 33,
    gid    => 'www-data',
  }

  postfix::mailalias { 'redmine':
    recipient => "|/usr/share/redmine/extra/mail_handler/rdm-mailhandler.rb --url '${url}' --key '${mailhandler_api_key}' --allow-override all", # lint:ignore:140chars -- command
  }

  file { '/var/www/robots.txt':
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0664',
    source => 'puppet:///modules/tails/redmine/robots.txt',
  }

  include tails::backupninja
  file { '/etc/backup.d/10.mysql':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "hotcopy = no
sqldump = yes
backupdir = /var/backups/mysql
databases = all
configfile = /etc/mysql/debian.cnf
",
    require => Package['backupninja'],
  }

}
