Facter.add('tails_mail_tls_fullchain') do
  setcode do
    file = '/etc/letsencrypt/live/mail.tails.boum.org/fullchain.pem'
    File.exists?(file) ? File.read(file) : nil
  end
end
