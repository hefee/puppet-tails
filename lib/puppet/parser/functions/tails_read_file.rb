require 'yaml'

module Puppet::Parser::Functions
  newfunction(:tails_read_file, :type => :rvalue) do |args|
    file = args[0]
    begin
      File.open(file).read
    rescue Errno::ENOENT, Errno::EACCES
      ""
    end
  end
end
