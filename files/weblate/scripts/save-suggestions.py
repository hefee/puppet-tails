#!/usr/bin/env python3

# This script generates a temporary copy of all translations including suggestions.
# TMPDIR - the directory where the temporary copy is located.
# We have created TMPDIR manually, by git cloning locally.
#
# Usage:
#
#   export DJANGO_SETTINGS_MODULE=weblate.settings
#   export DJANGO_IS_MANAGEMENT_COMMAND=1
#   /var/lib/weblate/scripts/save-suggestions.py

import logging
import logging.config

import subprocess
import sys

sys.path.append("/usr/local/share/weblate")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from weblate.trans.models import *

TMPDIR = "/var/lib/weblate/repositories/vcs/staging/"

logging.config.fileConfig('/var/lib/weblate/config/saveSuggestions.conf')

logger = logging.getLogger('saveSuggestions')

logger.info("Start a new run.")


def subprocessOutputToLogger(cmd, **kwargs):
    logger.info("Running command '%s':", " ".join(cmd))
    popen = subprocess.Popen(cmd, universal_newlines=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             **kwargs)
    for stdout_line in iter(popen.stdout.readline, ""):
        logger.debug(stdout_line.strip())
    popen.stdout.close()
    returncode = popen.wait()
    if returncode:
        raise subprocess.CalledProcessError(returncode, cmd)


def update_unit(translation, unit, target):
    # weblate/trans/models/translation.py: update_units
    src = unit.get_source_plurals()[0]
    add = False

    pounit, add = translation.store.find_unit(unit.context, src)

    # Bail out if we have not found anything
    if pounit is None or pounit.is_obsolete():
        logger.warning('message %s disappeared!', unit)

    pounit.set_target(target)     # update po file with first suggestion
    pounit.mark_fuzzy(False)           # mark change as non fuzzy


try:
    subprocessOutputToLogger(["git", "clean", "-fd"], cwd=TMPDIR)
    subprocessOutputToLogger(["git", "fetch"], cwd=TMPDIR)
    subprocessOutputToLogger(["git", "reset", 'FETCH_HEAD', "--hard"],
                             cwd=TMPDIR)
except:
    logger.exception("Got an exception")
    raise

logger.info("Start search for suggestions.")
len_subprojects = len(Component.objects.all())
subprojects = list(Component.objects.all())


for i in range(len_subprojects):
    subproject = subprojects[i]
    try:
        for translation in subproject.translation_set.all():
            changed = False
            for unit in translation.unit_set.all():
                if unit.suggestions:
                    # Get newest most voted suggestion
                    date_sorted = sorted(unit.suggestions, key=lambda i: i.timestamp, reverse=True)
                    s = sorted(date_sorted, key=lambda i: i.get_num_votes(), reverse=True)[0]
                    logger.debug("found suggestion for %s", unit)
                    update_unit(translation, unit, s.target)
                    changed = True
                elif unit.pending:
                    # Save uncommitted changes
                    logger.debug("uncommitted changes found for %s", unit)
                    update_unit(translation, unit, unit.target)
                    changed = True

            # save with suggestions
            if changed:
                with open(TMPDIR+"/"+translation.filename, "wb") as f:
                    translation.store.store.serialize(f)
    except:
        logger.exception("Got an exception for %s(%i)", subproject.name, i)
        raise

logger.info("Successfully updated %s.", TMPDIR)
