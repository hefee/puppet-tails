#!/bin/sh

# This script is called by the post_test_cleanup jenkins-job-builder builder
# defined in jenkins-jobs.git.

set -e
set -u
set -x

WORKSPACE="$1"
TEMP_DIR="$2"

[ -n "$WORKSPACE" ] || exit 2
[ -n "$TEMP_DIR" ] || exit 3
[ -d "$WORKSPACE" ] || exit 4
[ -d "$TEMP_DIR" ] || exit 5

sudo -n chown -R jenkins "${TEMP_DIR}"

mkdir -p "${WORKSPACE}/build-artifacts"

find "${TEMP_DIR}" \
	\( -name '*.png' -o -name '*.mkv' -o -name '*.pcap'      \
	   -o -name debug.log -o -name cucumber.json             \
	   -o -name '*_chutney-data' -o -name '*.htpdate'        \
	   -o -name '*.journal' -o -name '*.tor'                 \
	   -o -name '*.cmd_output_*' -o -name '*.file_content_*' \
	\) \
	-print0 | \
	xargs --null --no-run-if-empty \
		mv --target-directory="${WORKSPACE}/build-artifacts/"

sudo -n rm -rf "${TEMP_DIR}"/*
