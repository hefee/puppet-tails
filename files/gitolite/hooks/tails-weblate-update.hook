#!/usr/bin/env python3

# Update git hook to ensure that a user is only allowed to push modifications
# to existing files. Only works with gitolite, which sets the GL_USER
# environment variable according to the SSH key used for pushing.

# It is used to ensure that Weblate is only allowed to push po files.

import git
import json
import os
import pathlib
import re
import sys


class AllowedLanguages:
    def __init__(self, pathname):
        self.pathname = pathname
        self.result = None

    def load(self):
        if self.pathname.with_name('langs.json').exists():
            config = json.load(self.pathname.with_name('langs.json').open())
        else:
            path = self.pathname.parent.with_name("config")
            config = json.load((path/'langs.json').open())

        maingitlangs = config.get('mainlangs')
        additionallangs = config.get('additionallangs')

        self.result = maingitlangs + additionallangs

    def __iter__(self):
        if self.result is None:
            self.load()
        return iter(self.result)


# Gitolite sets env variable for the user for the SSH user
EXPECTED_GL_USER = "weblate"

# Only allowed committer for Weblate
EXPECTED_COMMITTER = git.Actor("Tails translators", "tails-l10n@boum.org")

# List of allowed language suffixes
ALLOWED_LANGUAGES = AllowedLanguages(pathlib.Path(os.path.realpath(__file__)))

# List of allowed directories, where Weblate is allowed to push; changes to
# all subdirectories are allowed, too
ALLOWED_DIRECTORIES = ['wiki/src/', ]


def validate_commit_range(repo, oldrev, newrev):
    """@returns issue, if there are reasons to block the update."""

    # get a list of commits
    rev_list = repo.iter_commits("{oldrev}..{newrev}".format(oldrev=oldrev, newrev=newrev))
    issues = []
    for commit in rev_list:
        if commit.committer != EXPECTED_COMMITTER:
            issues.append("{commit_hash} - [POLICY] Weblate is only allowed to push commits with '{expected_committer_name} <{expected_committer_email}>' as committer.".format(commit_hash=commit.hexsha[-10:], expected_committer_name=EXPECTED_COMMITTER.name, expected_committer_email=EXPECTED_COMMITTER.email))

    oldHead = repo.commit(oldrev)
    newHead = repo.commit(newrev)

    # Check list of modified files.
    for diff in oldHead.diff(newHead):
        fname = diff.b_path    # it is a complete path with directories based on the repo root

        # po file?
        if not fname.endswith(".po"):
            issues.append("[POLICY] Weblate is only allowed to modify po files.")
        # hidden file?
        elif fname.split("/")[-1].startswith("."):
            issues.append("[POLICY] Weblate is only allowed to modify po files.")
        # Known language extension?
        elif not any(fname.endswith(".{}.po".format(i)) for i in ALLOWED_LANGUAGES):
            issues.append("[POLICY] Language not allowed to push.")

        if not any(fname.startswith(i) for i in ALLOWED_DIRECTORIES):
            issues.append("[POLICY] Weblate is not allowed to push changes for {fname}.".format(fname=fname))
        if diff.b_mode != 0o100644:
            issues.append("[POLICY] Weblate is only allowed to push files with mode 100644, while {fname} has mode {mode:o}.".format(fname=fname, mode=diff.b_mode))

    return issues


def main(repopath):
    # sys.argv[1] is the name of the ref being updated. We configure
    # Gitolite to only allow the weblate user to push to the master branch,
    # so we don't need to duplicate this check here.

    # Similarly, we configure Gitolite to forbid the weblate user from
    # pushing anything that's not fast-forward, so here we don't need to
    # check if it tries to rewrite history.

    oldrev = sys.argv[2]
    newrev = sys.argv[3]

    if os.environ.get("GL_USER") == EXPECTED_GL_USER:
        if oldrev == "0000000000000000000000000000000000000000":
            sys.exit("[POLICY] Weblate is not allowed to create new branches.")

        if newrev == "0000000000000000000000000000000000000000":
            sys.exit("[POLICY] Weblate is not allowed to delete branches.")

        repo = git.Repo(repopath)
        issues = validate_commit_range(repo, oldrev, newrev)
        if issues:
            sys.exit("\n".join(issues))
    else:
        # We do not interfere with commits from other users
        pass


if __name__ == "__main__":
    main('')
